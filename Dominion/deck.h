#ifndef DECK_H
#define DECK_H

#include <vector>
#include "cardlist.h"


using std::vector;

class KDeck {
	public:
		void CreateKDeck(ACard card) {
			KDecklist.push_back(card);
		}
	private:
		vector<ACard> KDecklist;
};

class VDeck {
	public:
		//constructor for vdeck
		VDeck(){
			size = 0;
		}
		
		void CreateVDeck(VCard card) {
			VDecklist.push_back(card);
			size = size + 1;
		}
		
		//getter for size
		int get_size(){
			return size;
		}
	
		void printdeck();
	private:	
		vector<VCard> VDecklist; 
		int size;		
};

class TDeck {
	public:
		void CreateTDeck(TCard card) {
			TDecklist.push_back(card);
		}
	private:
		vector<TCard> TDecklist;
};

void VDeck_List(VDeck deck);

#endif
	
	


#include <iostream>
#include <vector>
#include <string>
#include "cardlist.h"
#include "deck.h"

using std::string;
using std::vector;
using std::cout;
using std::endl;


void VDeck_List(VDeck deck) {
	for(int i = 0; i < 8; i++){
		VCard card("Estate", 1);
		deck.CreateVDeck(card);
	}
	for(int i = 0; i < 8; i++){
		VCard card("Duchy", 3);
		deck.CreateVDeck(card);
	}		 			

	for(int i = 0; i < 8; i++){
		VCard card("Province", 6);
		deck.CreateVDeck(card);
	}		 
}		

//checking if it stored vector correctly
void VDeck::printdeck() {
	int counter = 0;
	cout << "in printdeck" << endl;
	for(int i = 0; i < get_size(); i++){
		cout << VDecklist[i].get_name() << " " << VDecklist[i].get_value() << endl;
	}
}				


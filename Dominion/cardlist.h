#ifndef CARDLIST_H
#define CARDLIST_H

#include <string>

using namespace::std;
//treasure card class
class TCard {
	public:
		//constructor for treasure card
		TCard(string name, int value) {
			name = name;
			value = value;
		}
		
	private:
		//name and value of treasure card	
		string name;
		int value;
};

//victory card class
class VCard {
	public:	
		//constructor for victory card
		VCard(string a, int b){
			name = a;
			value = b; 
		}
	
		//getters for vcard
		string get_name(){
			return name;
		}
		int get_value(){
			return value;
		}
	private:
		//name and value for victory card
		string name;
		int value;
};			

//action card class
class ACard {
	public:
		//constructor for action card
		ACard(string name, int cost, int drawPower, int action, int buy, int tempGold) {
			cost = cost;
			drawPower = drawPower;
			action = action;
			buy = buy;
			tempGold = tempGold;
		}
	
	private:
		//name, +draw, +action, +buy and temporary gold for the action card
		string name;
		int cost;
		int drawPower;
		int action;
		int buy;
		int tempGold;	
};

#endif
